<?php 
session_start();

if (!isset($_SESSION['loggedin'])) {

	header('Location: index.php');

	exit();

}

include $_SERVER['DOCUMENT_ROOT']."/include/constant.php";

$conn = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);

if (mysqli_connect_errno()) {

	die(header('Location: error?error=0001'));

}


?>

<!DOCTYPE html>
<html>
    <head>
        <?php include $PATH."/include/headerhtml.php"?>
    </head>

    <body>
        <?php include $PATH."/include/header.php"?>

        <div class="container fill arg">

            <div class="row">
                <div class="col">
                <h4>Account table</h4>
                    <table class="table">
                        <thead>
                            <tr>
                            <th scope="col">id</th>
                            <th scope="col">username</th>
                            <th scope="col">password</th>
                            <th scope="col">date</th>
                            <th scope="col">grade</th>
                            </tr>
                        </thead>
                        <tbody>

                    <?php
                        $sql="SELECT * from account";

                        if ($stmt = $conn->prepare($sql)) {

                            $stmt->execute();
                        
                            $stmt->store_result();
                        
                        }
                        
                        if ($stmt->num_rows > 0) {
                        
                            $stmt->bind_result($id, $username, $password, $date ,$grade);
                        
                            while($stmt->fetch()){
                        
                                echo '
                                <tr>
                                    <td>'.$id.'</td>
                                    <td>'.$username.'</td>
                                    <td>'.$password.'</td>
                                    <td>'.$date.'</td>
                                    <td>'.$grade.'</td>
                                </tr>
                                ';
                        
                            }
                        
                        }
                    ?>
                            </tbody>
                        </table>
                </div>
            </div>

            <div class="row">
                <div class="col">
                <h4>Comment table</h4>
                <table class="table">
                        <thead>
                            <tr>
                            <th scope="col">id</th>
                            <th scope="col">message</th>
                            <th scope="col">date</th>
                            <th scope="col">owner</th>
                            <th scope="col">post</th>
                            </tr>
                        </thead>
                        <tbody>

                    <?php
                        $sql="SELECT * from comments";

                        if ($stmt = $conn->prepare($sql)) {

                            $stmt->execute();
                        
                            $stmt->store_result();
                        
                        }
                        
                        if ($stmt->num_rows > 0) {
                        
                            $stmt->bind_result($id_com, $msg_com, $date_com, $owner_com ,$post_com);
                        
                            while($stmt->fetch()){
                        
                                echo '
                                <tr>
                                    <td>'.$id_com.'</td>
                                    <td>'.$msg_com.'</td>
                                    <td>'.$date_com.'</td>
                                    <td>'.$owner_com.'</td>
                                    <td>'.$post_com.'</td>
                                </tr>
                                ';
                        
                            }
                        
                        }
                    ?>
                            </tbody>
                        </table>

                </div>
            </div>

            <div class="row">
                <div class="col">
                    <h4>Post table</h4>
                <table class="table">
                        <thead>
                            <tr>
                            <th scope="col">id</th>
                            <th scope="col">title</th>
                            <th scope="col">message</th>
                            <th scope="col">date</th>
                            <th scope="col">owner</th>
                            </tr>
                        </thead>
                        <tbody>

                    <?php
                        $sql="SELECT * from post";

                        if ($stmt = $conn->prepare($sql)) {

                            $stmt->execute();
                        
                            $stmt->store_result();
                        
                        }
                        
                        if ($stmt->num_rows > 0) {
                        
                            $stmt->bind_result($id_post, $title_post, $msg_post, $date_post ,$owner_post);
                        
                            while($stmt->fetch()){
                        
                                echo '
                                <tr>
                                    <td>'.$id_post.'</td>
                                    <td>'.$title_post.'</td>
                                    <td>'.$msg_post.'</td>
                                    <td>'.$date_post.'</td>
                                    <td>'.$owner_post.'</td>
                                </tr>
                                ';
                        
                            }
                        
                        }
                    ?>
                            </tbody>
                        </table>

                </div>
            </div>


    </body>

</html>