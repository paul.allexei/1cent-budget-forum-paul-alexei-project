<?php 
session_start();

if (!isset($_SESSION['loggedin'])) {

	header('Location: index.php');

	exit();

}

$post=$_GET['id'];

include $_SERVER['DOCUMENT_ROOT']."/include/constant.php";

$conn = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);

if (mysqli_connect_errno()) {

	die(header('Location: error?error=0001'));

}

$sql="SELECT title_post,msg_post,date_post,owner_post from post WHERE id_post=?";

if ($stmt = $conn->prepare($sql)) {

    $stmt->bind_param("i",$post);

    $stmt->execute();

    $stmt->store_result();

}

if ($stmt->num_rows > 0) {

    $stmt->bind_result($title, $msg, $date, $poster);

    $stmt->fetch();
}

?>

<!DOCTYPE html>
<html>
    <head>
        <?php include $PATH."/include/headerhtml.php"?>
    </head>

    <body>
        <?php include $PATH."/include/header.php"?>

        <div class="container fill arg">
            <div class="row">
                <div class="col">
                    <h2><?php echo $title;?></h2>

                    <p><?php echo $msg;?></p>
                   
                    
                    <?php
                        if($poster==$_SESSION['id'] || $_SESSION['grade']==3){
                            echo '
                            <a href="'.$SITENAME.'/action/deletepost?id='.$post.'" class="btn btn-info">Delete</a>
                            <a href="'.$SITENAME.'/editpost?id='.$post.'" class="btn btn-info">Edit</a>';
                        }
                    ?>
                    
                </div>
            </div>
            <div class="row">
                <div class="col">
               

<?php

$sql="SELECT msg_com, owner_com ,date_com,id_com FROM comments WHERE post_com=?";

if ($stmt = $conn->prepare($sql)) {

    $stmt->bind_param("i",$_GET['id']);

    $stmt->execute();

    $stmt->store_result();

}

if ($stmt->num_rows > 0) {

    $stmt->bind_result($msg, $owner, $date,$idm);

    while($stmt->fetch()){

        echo $owner.' <br/> '.$msg.' <br/> '.$date.'<br/>';

        if($owner == $_SESSION['id'] || $_SESSION['grade']==3){
            echo '<a href="'.$SITENAME.'/action/deletecomment?id='.$idm.'&return='.$post.'" class="btn btn-info">Delete comm</a>';
            echo '<a href="'.$SITENAME.'/editcomment?id='.$idm.'&return='.$post.'" class="btn btn-info">Edit comm</a>';
        }

    }

}else{

    echo 'No comments';

}
?>
                    

                </div>
            </div>
            <div class="row sendresponse" style="position: sticky;top: 10000px;">

                        <form method="post" id="sendmsg" action="<?php echo $SITENAME;?>/action/createcomment">

                            <textarea id="msg" class="form-control" name="msg" placeholder="your message" style="display: inline-block;width: 80%;"></textarea>

                            <input type="hidden" id="post" name="post" value="<?php echo $post;?>">

                            <input type="submit" id="sendresponse" class="btn btn-primary" value="Send">

                        </form>

                    </div> 
        </div>


    </body>

</html>