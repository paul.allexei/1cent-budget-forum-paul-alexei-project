<?php include $_SERVER['DOCUMENT_ROOT']."/include/constant.php";
?>

<!DOCTYPE html>
<html>
    <head>
        <?php include $PATH."/include/headerhtml.php"?>
    </head>

    <body>


        <div class="container main justify-content-center">

            <div class="row title">

                <div class="col">
                    <img src="images/logo.png" class="image-log"/>
                    <h3>Login</h3>
                </div>

            </div>

            <div class="row form">

                <div class="col">

                    <form action="action/login" method="post">
                        <input type="text" id="login" class="form-control" name="username" placeholder="Username" required>
                        <input type="password" id="password" class="form-control" name="password" placeholder="Password" required>
                        <input type="submit" class="btn btn-primary" value="Log In">
                    </form>

                </div>

            </div>

            <div class="row footer">

                <div class="col">
                    <a class="underlineHover" href="register">Don't have an account?</a>
                </div>

            </div>

        </div>

</body>

</html>