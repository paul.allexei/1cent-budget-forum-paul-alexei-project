<?php 
session_start();

if (!isset($_SESSION['loggedin'])) {

	header('Location: index.php');

	exit();

}


include $_SERVER['DOCUMENT_ROOT']."/include/constant.php";

$conn = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);

if (mysqli_connect_errno()) {

	die(header('Location: error?error=0001'));

}

$sql="SELECT password,date from account WHERE id=?";

if ($stmt = $conn->prepare($sql)) {

    $stmt->bind_param("i",$_SESSION['id']);

    $stmt->execute();

    $stmt->store_result();

}

if ($stmt->num_rows > 0) {

    $stmt->bind_result($pass,$date);

    $stmt->fetch();
}

?>

<!DOCTYPE html>
<html>
    <head>
        <?php include $PATH."/include/headerhtml.php"?>
    </head>

    <body>
        <?php include $PATH."/include/header.php"?>

        <div class="container fill arg">
            <div class="row">
                <div class="col">
                    <h2><?php echo $_SESSION['name'];?></h2>

                    <p>Account id: <?php echo $_SESSION['id'];?></p>
                    
                    <p>Account grade: <?php 
                        if($_SESSION['grade']==3){
                            echo "admin";
                        }else{
                            echo "user";
                        }
                   ?></p>
                    
                    <p>Account password: <?php echo $pass;?></p>
                    
                    <p>Account registered: <?php echo $date;?></p>
                
                    <p>Change Password:</p>
                    <form action="<?php echo $SITENAME;?>/action/changepassword" method="post">
                        <input type="text" class="form-control" name="newpassword" value="<?php echo $pass;?>" required>
                        <input type="submit" class="btn btn-primary" value="Change password">
                    </form>
                    
                </div>
            </div>


    </body>

</html>