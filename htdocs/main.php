<?php 
session_start();

if (!isset($_SESSION['loggedin'])) {

	header('Location: index.php');

	exit();

}

include $_SERVER['DOCUMENT_ROOT']."/include/constant.php";

$conn = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);

if (mysqli_connect_errno()) {

	die(header('Location: error?error=0001'));

}
?>

<!DOCTYPE html>
<html>
    <head>
        <?php include $PATH."/include/headerhtml.php"?>
    </head>

    <body>
        <?php include $PATH."/include/header.php"?>

        <div class="container fill arg">
            <div class="row">
                <div class="col">
                    <a href="<?php echo $SITENAME;?>/newpost" class="btn btn-info">New Post</a>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <ul>

<?php

$sql="Select id_post,title_post,msg_post,date_post from post ORDER BY date_post DESC";

if ($stmt = $conn->prepare($sql)) {

    $stmt->execute();

    $stmt->store_result();

}

if ($stmt->num_rows > 0) {

    $stmt->bind_result($id, $title, $msg, $date);

    while($stmt->fetch()){

        echo '

            <a href="'.$SITENAME.'/post?id='.$id.'">
            <li>
                '.$title.' | '.$msg.' | '.$date.'
            </li>
            </a>

        ';

    }

}else{

    echo '<li>Nothing Found</li>';

}
?>

                    </ul>
                </div>
            </div>
        </div>


    </body>

</html>