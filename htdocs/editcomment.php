<?php 
session_start();

if (!isset($_SESSION['loggedin'])) {

	header('Location: index.php');

	exit();

}

include $_SERVER['DOCUMENT_ROOT']."/include/constant.php";

$conn = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);

if (mysqli_connect_errno()) {

	die(header('Location: error?error=0001'));

}


$sql="SELECT msg_com from comments WHERE id_com=?";

if ($stmt = $conn->prepare($sql)) {

    $stmt->bind_param("i",$_GET['id']);

    $stmt->execute();

    $stmt->store_result();

}

if ($stmt->num_rows > 0) {

    $stmt->bind_result($msg);

    $stmt->fetch();
}

?>

<!DOCTYPE html>
<html>
    <head>
        <?php include $PATH."/include/headerhtml.php"?>
    </head>

    <body>
        <?php include $PATH."/include/header.php"?>

        <div class="container fill arg">
            <div class="row">
                <div class="col">
                    <ul>

                        <form action="<?php echo $SITENAME;?>/action/editcomment" method="post">
                            <input type="text" id="msg" class="form-control" name="msg" placeholder="Message" value="<?php echo $msg?>"  required>
                            <input type="hidden" name="comm" value="<?php echo $_GET['id']; ?>">
                            <input type="hidden" name="ret" value="<?php echo $_GET['return']; ?>">
                            <input type="submit" class="btn btn-primary" value="Edit comment">
                        </form>

                    </ul>
                </div>
            </div>
        </div>


    </body>

</html>