<?php 

session_start();

if (!isset($_SESSION['loggedin'])) {

	header('Location: index.php');

	exit();

}
$msg=$_POST['msg'];
$session=$_SESSION['id'];
$post=$_POST['post'];

include $_SERVER['DOCUMENT_ROOT']."/include/constant.php";

$conn = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);

if (mysqli_connect_errno()) {

	die(header('Location: error?error=0001'));

}

$stmt = $conn->prepare("INSERT INTO comments(msg_com,post_com,owner_com) VALUES(?,?,?)");

$stmt->bind_param("sii", $msg, $post, $_SESSION['id']);

$stmt->execute();

header('Location: ../post.php?id='.$post);

?>
