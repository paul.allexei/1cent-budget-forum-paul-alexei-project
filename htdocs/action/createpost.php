<?php 

session_start();

if (!isset($_SESSION['loggedin'])) {

	header('Location: index.php');

	exit();

}
$title=$_POST['title'];
$msg=$_POST['msg'];
$session=$_SESSION['id'];

include $_SERVER['DOCUMENT_ROOT']."/include/constant.php";

$conn = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);

if (mysqli_connect_errno()) {

	die(header('Location: error?error=0001'));

}

$stmt = $conn->prepare('INSERT INTO post(title_post,msg_post,owner_post) VALUES(?,?,?)');

$stmt->bind_param("ssi", $title, $msg, $session);

$stmt->execute();

header('Location: ../main');

?>