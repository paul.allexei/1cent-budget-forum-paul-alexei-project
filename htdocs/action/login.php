<?php

session_start();

include $_SERVER['DOCUMENT_ROOT']."/include/constant.php";



$con = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);

if ( mysqli_connect_errno() ) {

	die(header('Location: index?error=0001'));

}


if ( !isset($_POST['username'], $_POST['password']) ) {

	die ('Please fill both the username and password field!');

}


if ($stmt = $con->prepare('SELECT id, date, password, grade FROM account WHERE username = ?')) {

	$stmt->bind_param('s', $_POST['username']);

	$stmt->execute();

	$stmt->store_result();

}

if ($stmt->num_rows > 0) {

	$stmt->bind_result($id, $date, $password,$grade);

	$stmt->fetch();

	if ($_POST['password'] == $password) {

			session_regenerate_id();

			$_SESSION['loggedin'] = TRUE;

			$_SESSION['name'] = strtoupper ($_POST['username']);

			$_SESSION['id'] = $id;
			
			$_SESSION['grade'] = $grade;


			header('Location: ../main.php');

	} else {

		header('Location: ../index.php?error=pas');

	}

} else {

	header('Location: ../index.php?error=usr');

}

$stmt->close();

?>