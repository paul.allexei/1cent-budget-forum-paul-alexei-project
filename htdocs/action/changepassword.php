<?php 

session_start();

if (!isset($_SESSION['loggedin'])) {

	header('Location: index.php');

	exit();

}
$password=$_POST['newpassword'];


include $_SERVER['DOCUMENT_ROOT']."/include/constant.php";

$conn = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);

if (mysqli_connect_errno()) {

	die(header('Location: error?error=0001'));

}

$stmt = $conn->prepare('UPDATE account SET password=? WHERE id=?');

$stmt->bind_param("si", $password, $_SESSION['id']);

$stmt->execute();

header('Location: ../profile');

?>