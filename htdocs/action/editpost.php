<?php 

session_start();

if (!isset($_SESSION['loggedin'])) {

	header('Location: index.php');

	exit();

}
$title=$_POST['title'];
$msg=$_POST['msg'];
$post=$_POST['post'];

include $_SERVER['DOCUMENT_ROOT']."/include/constant.php";

$conn = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);

if (mysqli_connect_errno()) {

	die(header('Location: error?error=0001'));

}

$stmt = $conn->prepare('UPDATE post SET title_post=?,msg_post=? WHERE id_post=?');

$stmt->bind_param("ssi", $title, $msg, $post);

$stmt->execute();

header('Location: ../main');

?>