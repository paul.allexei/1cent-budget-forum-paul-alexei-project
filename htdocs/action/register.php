<?php
if ( !isset($_POST['username'], $_POST['password']) ) {

	die ('Please fill both the username and password field!');

}
session_start();

include $_SERVER['DOCUMENT_ROOT']."/include/constant.php";


$con = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);

if ( mysqli_connect_errno() ) {

    die(header('Location: index?error=0001'));
}


if ($stmt = $con->prepare('SELECT id FROM account WHERE username = ?')) {

	$stmt->bind_param('s', $_POST['username']);

	$stmt->execute();

	$stmt->store_result();

}

if ($stmt->num_rows == 0) {

	$stmt->fetch();

    session_regenerate_id();
    
    $stmt = $con->prepare('INSERT INTO account(username, password) VALUES(?,?)');

    $stmt->bind_param('ss', $_POST['username'],$_POST['password']);

    $stmt->execute();

    $_SESSION['loggedin'] = TRUE;

    $_SESSION['name'] = strtoupper ($_POST['username']);

    $_SESSION['id'] = $id;

    header('Location: ../main.php');

} else {

	header('Location: ../index.php?error=usr');

}

?>