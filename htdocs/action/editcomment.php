<?php 

session_start();

if (!isset($_SESSION['loggedin'])) {

	header('Location: index.php');

	exit();

}
$msg=$_POST['msg'];
$comm=$_POST['comm'];

include $_SERVER['DOCUMENT_ROOT']."/include/constant.php";

$conn = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);

if (mysqli_connect_errno()) {

	die(header('Location: error?error=0001'));

}

$stmt = $conn->prepare('UPDATE comments SET msg_com=? WHERE id_com=?');

$stmt->bind_param("si", $msg, $comm);

$stmt->execute();

header('Location: ../post?id='.$_POST['ret']);

?>