<?php 
session_start();

if (!isset($_SESSION['loggedin'])) {

	header('Location: index.php');

	exit();

}

include $_SERVER['DOCUMENT_ROOT']."/include/constant.php";

$conn = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);

if (mysqli_connect_errno()) {

	die(header('Location: error?error=0001'));

}


$sql="SELECT title_post,msg_post,date_post from post WHERE id_post=?";

if ($stmt = $conn->prepare($sql)) {

    $stmt->bind_param("i",$_GET['id']);

    $stmt->execute();

    $stmt->store_result();

}

if ($stmt->num_rows > 0) {

    $stmt->bind_result($title, $msg, $date);

    $stmt->fetch();
}

?>

<!DOCTYPE html>
<html>
    <head>
        <?php include $PATH."/include/headerhtml.php"?>
    </head>

    <body>
        <?php include $PATH."/include/header.php"?>

        <div class="container fill arg">
            <div class="row">
                <div class="col">
                    <ul>

                        <form action="<?php echo $SITENAME;?>/action/editpost" method="post">
                            <input type="text" id="title" class="form-control" name="title" placeholder="Title" value="<?php echo $title?>" required>
                            <input type="text" id="msg" class="form-control" name="msg" placeholder="Message" value="<?php echo $msg?>"  required>
                            <input type="hidden" name="post" value="<?php echo $_GET['id']; ?>">
                            <input type="submit" class="btn btn-primary" value="Create post">
                        </form>

                    </ul>
                </div>
            </div>
        </div>


    </body>

</html>