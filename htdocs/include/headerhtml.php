<?php 

echo '
		<title>Paul Alexei Project</title>

		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" /><meta charset="utf-8" />

		<link rel="icon" href="'.$SITENAME.'/images/favicon.ico" type="image/gif" sizes="16x16">		

		<link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet"> 

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

		<link rel="stylesheet" href="'.$SITENAME.'/resource/index.css" />

		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

';

?>